(function ($) {
  'use strict';

  $('.fi-add_heading').click(function () {
    $(this).siblings('.fi-condition').slideToggle();
  })

  $('#mobile-menuToggle input').click(function () {
    var mmenu = $('#mobile-menu');
    if ($(this).is(':checked')) {
      mmenu.addClass('active');
    } else {
      mmenu.removeClass('active');
    }
  })

  $(window).on({
    load: function () {
      var catCarousel = $('#category-carousel'),
        mainSlider = $('#main-slider'),
        window_width = $(window).width();

      mainSlider.slick({
        infinite: true,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 5000,
        speed: 800,
        slidesToShow: 1,
        cssEase: 'ease-in-out',
        adaptiveHeight: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              dots: true,
            }
          }
        ]
      });

      if (window_width < 992) {
        catCarousel.slick({
          infinite: false,
          arrows: false,
          speed: 300,
          slidesToShow: 5,
          mobileFirst: true,
          variableWidth: true,
          swipeToSlide: true,
          infinite: true,
          cssEase: 'ease-in-out',
          adaptiveHeight: true,

          responsive: [
            {
              breakpoint: 762,
              settings: {
                slidesToShow: 4,
              }
            },
            {
              breakpoint: 592,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 592,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 452,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 352,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
        });
      }
    }
  });

})(jQuery);